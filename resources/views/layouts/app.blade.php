<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Houses</title>
    <link href="{!! asset('css/app.css') !!}" media="all" rel="stylesheet" type="text/css" />
</head>
<body>
<nav class="navbar navbar-expand-sm bg-info navbar-light" style="margin-bottom: 20px;">
    <a class="navbar-brand">Houses</a>
</nav>
<div class="container-fluid" id="app">
    @yield('content')
</div>
<script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
</body>
</html>
