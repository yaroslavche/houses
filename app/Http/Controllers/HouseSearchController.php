<?php

namespace App\Http\Controllers;

use App\House;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class HouseSearchController extends Controller
{
    const SEARCH_RESULTS_LIMIT = 12;

    /**
     * Search houses
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request): JsonResponse
    {
        $searchResult = $this->prepareFilters($request)->limit(self::SEARCH_RESULTS_LIMIT)->get();

        // Needs for pagination, but pagination no need implement - return just info. Yes, one extra query at this time.
        // Can be easily removed. It's not even used on frontend =)
        $total = $this->prepareFilters($request)->selectRaw('COUNT(*) as total_count')
            ->get(['total_count'])->first()->toArray();

        return Response::json(['results' => $searchResult, 'total' => $total['total_count']]);
    }

    /**
     * prepare filters from request parameters.
     *
     * @param Request $request
     * @return Builder
     */
    private function prepareFilters(Request $request): Builder
    {
        $name = $request->get('name', null);
        $priceFrom = $request->get('priceFrom', null);
        $priceTo = $request->get('priceTo', null);
        $bedrooms = $request->get('bedrooms', null);
        $bathrooms = $request->get('bathrooms', null);
        $storeys = $request->get('storeys', null);
        $garages = $request->get('garages', null);

        $priceFrom = $priceFrom === null ? null : abs((float)$priceFrom);
        $priceTo = $priceTo === null ? null : abs((float)$priceTo);
        $bedrooms = $bedrooms === null ? null : abs((int)$bedrooms);
        $bathrooms = $bathrooms === null ? null : abs((int)$bathrooms);
        $storeys = $storeys === null ? null : abs((int)$storeys);
        $garages = $garages === null ? null : abs((int)$garages);

        /**
         * @var Builder $builder
         */
        $builder = House::query();

        if (null !== $name) {
            $builder->where('name', 'like', sprintf('%%%s%%', $name));
        }

        // check valid prices: priceTo > priceFrom > 0
        if ($priceFrom > 0 && $priceTo > $priceFrom) {
            $builder->whereBetween('price', [$priceFrom, $priceTo]);
        } elseif ($priceFrom > 0) {
            if ($priceFrom === $priceTo) {
                $builder->where('price', $priceFrom);
            } else {
                $builder->where('price', '>=', $priceFrom);
            }
        } elseif ($priceTo > 0) {
            $builder->where('price', '<=', $priceTo);
        }

        if (null !== $bedrooms) {
            $builder->where('bedrooms', $bedrooms);
        }

        if (null !== $bathrooms) {
            $builder->where('bathrooms', $bathrooms);
        }

        if (null !== $storeys) {
            $builder->where('storeys', $storeys);
        }

        if (null !== $garages) {
            $builder->where('garages', $garages);
        }

        return $builder;
    }

    /**
     * Collect available search ranges
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function getSearchRanges(Request $request): JsonResponse
    {
        /**
         * @var Builder $builder
         */
        $builder = House::query();
        $builder->selectRaw(
            'MIN(price) as price_min, MAX(price) as price_max, ' .
            'MIN(bedrooms) as bedrooms_min, MAX(bedrooms) as bedrooms_max, ' .
            'MIN(bathrooms) as bathrooms_min, MAX(bathrooms) as bathrooms_max, ' .
            'MIN(storeys) as storeys_min, MAX(storeys) as storeys_max, ' .
            'MIN(garages) as garages_min, MAX(garages) as garages_max'
        );
        $ranges = $builder->get([
            'price_min', 'price_max',
            'bedrooms_min', 'bedrooms_max',
            'bathrooms_min', 'bathrooms_max',
            'storeys_min', 'storeys_max',
            'garages_min', 'garages_max',
        ])->first()->toArray();
        $rangesResponseArray = [
            'price' => ['min' => $ranges['price_min'], 'max' => $ranges['price_max']],
            'bedrooms' => ['min' => $ranges['bedrooms_min'], 'max' => $ranges['bedrooms_max']],
            'bathrooms' => ['min' => $ranges['bathrooms_min'], 'max' => $ranges['bathrooms_max']],
            'storeys' => ['min' => $ranges['storeys_min'], 'max' => $ranges['storeys_max']],
            'garages' => ['min' => $ranges['garages_min'], 'max' => $ranges['garages_max']],
        ];
        return Response::json(['ranges' => $rangesResponseArray]);
    }
}
