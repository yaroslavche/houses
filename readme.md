# PHP Developer Test

## Installation
Install vendors, copy `.env.example` to `.env` and set key:
```bash
$ git clone git@bitbucket.org:yaroslavche/houses.git
$ composer install
$ cp .env.example .env
$ php artisan key:generate
```
Configure database connection in `.env`.
If want - rebuild `app.js` (already builded):
```bash
$ npm install
$ npm run dev
```

Then
```bash
$ php artisan migrate
$ php artisan db:seed
$ php artisan serve
```

And go to [http://localhost:8000](http://localhost:8000)
