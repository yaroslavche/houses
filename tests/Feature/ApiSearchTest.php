<?php

namespace Tests\Feature;

use Tests\TestCase;

class ApiSearchTest extends TestCase
{
    /**
     * @test
     * @return void
     */
    public function searchTest()
    {
        $response = $this->json('POST', '/api/search', ['name' => 'space']);

        $response->assertStatus(200);

        $response->assertJsonStructure(['results' => []]);
    }

    /**
     * @test
     * @return void
     */
    public function getSearchRangesTest()
    {
        $response = $this->get('/api/get_search_ranges');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'ranges' => [
                'price' => ['min', 'max'],
                'bedrooms' => ['min', 'max'],
                'bathrooms' => ['min', 'max'],
                'storeys' => ['min', 'max'],
                'garages' => ['min', 'max']
            ]
        ]);
    }
}
