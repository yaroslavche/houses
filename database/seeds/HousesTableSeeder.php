<?php

use App\House;
use Illuminate\Database\Seeder;

class HousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * Data fetched from given csv-formatted file stored in resources/data/property-data.csv
     * Assume that csv has column names
     *
     * @return void
     */
    public function run()
    {
        House::truncate();

        $dataFilePath = 'resources/data/property-data.csv';
        $data = array_map('str_getcsv', file($dataFilePath));

        // first element must be column names, other - data
        $columns = array_shift($data);

        // specify indexes in data array
        $nameIndex = array_search('Name', $columns);
        $priceIndex = array_search('Price', $columns);
        $bedroomsIndex = array_search('Bedrooms', $columns);
        $bathroomsIndex = array_search('Bathrooms', $columns);
        $storeysIndex = array_search('Storeys', $columns);
        $garagesIndex = array_search('Garages', $columns);

        // if some columns not present - will return void with output console message
        if (
            $nameIndex === false ||
            $priceIndex === false ||
            $bedroomsIndex === false ||
            $bathroomsIndex === false ||
            $storeysIndex === false ||
            $garagesIndex === false
        ) {
            $this->command->info('Some data columns are missed. Please check csv file. No data will written.');
            return;
        }

        foreach ($data as $house) {
            House::create([
                'name' => $house[$nameIndex],
                'price' => (float)$house[$priceIndex],
                'bedrooms' => (int)$house[$bedroomsIndex],
                'bathrooms' => (int)$house[$bathroomsIndex],
                'storeys' => (int)$house[$storeysIndex],
                'garages' => (int)$house[$garagesIndex]
            ]);
        }
    }
}
